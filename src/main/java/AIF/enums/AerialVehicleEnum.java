package AIF.enums;

public enum AerialVehicleEnum {

    F15("F15"),
    F16("F16"),
    EITAN("Eitan"),
    SHOVAL("Shoval"),
    ZIK("Zik"),
    KOCHAV("Kochav");

    String name;

    AerialVehicleEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
