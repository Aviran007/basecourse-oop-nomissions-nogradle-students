package AIF.enums;

public enum EquipmentEnum {

    ATTACK("Attack"),
    BDA("BDA"),
    SENSOR("Sensor");

    String name;

    EquipmentEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
