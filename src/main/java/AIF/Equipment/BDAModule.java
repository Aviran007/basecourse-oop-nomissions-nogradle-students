package AIF.Equipment;

import AIF.AerialVehicles.UAV.Haron.Eitan;
import AIF.enums.EquipmentEnum;

public class BDAModule implements Equipment {
    public static final int RANGE = 300;

    @Override
    public void activate() {

    }

    @Override
    public String toString() {
        return EquipmentEnum.BDA.getName();
    }

}
