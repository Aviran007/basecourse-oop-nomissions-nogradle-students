package AIF.Equipment;

import AIF.enums.EquipmentEnum;

public class SensorModule implements Equipment {
    public static final int RANGE = 600;

    @Override
    public void activate() {

    }

    @Override
    public String toString() {
        return EquipmentEnum.SENSOR.getName();
    }

}
