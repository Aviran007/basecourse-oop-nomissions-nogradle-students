package AIF.Equipment;


import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.enums.EquipmentEnum;

public class AttackModule implements Equipment {
    public static final int RANGE = 1500;
    private boolean fired;

    public boolean isFired() {
        return fired;
    }

    public AttackModule() {
        this.fired = false;
    }

    @Override
    public void activate() throws NoModuleCanPerformException {
        if (!fired) {
            fired = true;

        } else {
            throw new NoModuleCanPerformException();
        }
    }

    @Override
    public String toString() {
        return EquipmentEnum.ATTACK.getName();
    }

}
