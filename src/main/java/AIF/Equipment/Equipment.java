package AIF.Equipment;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;

public interface Equipment {
    void activate() throws NoModuleCanPerformException;
}
