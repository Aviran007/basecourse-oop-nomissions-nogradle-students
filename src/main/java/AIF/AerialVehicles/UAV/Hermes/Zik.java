package AIF.AerialVehicles.UAV.Hermes;


import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class Zik extends Hermes {
    private static final int STATION_NUM = 1;

    public Zik(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.SENSOR, EquipmentEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.ZIK.getName();
    }

}