package AIF.AerialVehicles.UAV.Hermes;

import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    public static final int MAINTENANCE_LIMIT = 10000;

    public Hermes(Coordinates currentLocation) {
        super(currentLocation);
    }

    public int getMaintenanceLimitKM() {
        return MAINTENANCE_LIMIT;
    }
}
