package AIF.AerialVehicles.UAV.Hermes;

import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class Kochav extends Hermes {
    private static final int STATION_NUM = 5;

    public Kochav(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.ATTACK, EquipmentEnum.SENSOR, EquipmentEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.KOCHAV.getName();
    }
}
