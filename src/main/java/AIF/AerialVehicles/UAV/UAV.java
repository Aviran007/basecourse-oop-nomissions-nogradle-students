package AIF.AerialVehicles.UAV;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {
    private static final int HOVER_OVER_LOCATION = 150;

    public UAV(Coordinates currentLocation) {
        super(currentLocation);
    }

    /**
     * Hover over a specific location
     *
     * @param hours how many hours should stay in the area
     * @throws CannotPerformOnGroundException exception
     */
    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if (!onGround) {
            System.out.println("Hovering over: " + currentLocation.getLongitude() + ','
                    + currentLocation.getLatitude());
            lastMaintenance += HOVER_OVER_LOCATION * hours;
        } else {
            throw new CannotPerformOnGroundException();
        }
    }
}
