package AIF.AerialVehicles.UAV.Haron;

import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;


public abstract class Haron extends UAV {
    public static final int MAINTENANCE_LIMIT = 15000;

    public Haron(Coordinates currentLocation) {
        super(currentLocation);
    }

    public int getMaintenanceLimitKM() {
        return MAINTENANCE_LIMIT;
    }
}
