package AIF.AerialVehicles.UAV.Haron;


import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class Shoval extends Haron {
    private static final int STATION_NUM = 3;

    public Shoval(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.SENSOR, EquipmentEnum.BDA, EquipmentEnum.ATTACK};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.SHOVAL.getName();
    }
}

