package AIF.AerialVehicles.UAV.Haron;

import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class Eitan extends Haron {

    private static final int STATION_NUM = 4;

    public Eitan(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.SENSOR, EquipmentEnum.ATTACK};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.EITAN.getName();
    }

}
