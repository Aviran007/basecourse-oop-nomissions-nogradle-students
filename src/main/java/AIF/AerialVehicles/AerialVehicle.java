package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Equipment.Equipment;
import AIF.enums.EquipmentEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author AviranG
 */
public abstract class AerialVehicle {

    protected boolean onGround;
    protected double lastMaintenance;
    protected Coordinates currentLocation;
    protected EquipmentEnum[] compatibleEquipments;
    protected List<Equipment> equipments;

    public AerialVehicle(Coordinates currentLocation) {
        this.onGround = true;
        this.lastMaintenance = 0;
        this.currentLocation = currentLocation;
        equipments = new ArrayList<>();
    }

    public abstract int getMaintenanceLimitKM();

    public abstract int getStationNum();

    public void takeOff()
            throws CannotPerformInMidAirException, NeedMaintenanceException {
        if (!onGround)
            throw new CannotPerformInMidAirException();
        else if (needMaintenance())
            throw new NeedMaintenanceException();
        else {
            System.out.println("Taking off");
            onGround = false;
        }
    }


    public void flyTo(Coordinates destination)
            throws CannotPerformOnGroundException {
        if (!onGround) {
            System.out.println("Flying to: " + destination.getLongitude() + ", " + destination.getLatitude());
            lastMaintenance += currentLocation.distance(destination);
        } else {
            throw new CannotPerformOnGroundException();
        }
    }


    public void land()
            throws CannotPerformOnGroundException {
        if (!onGround) {
            System.out.println("Landing");
            onGround = true;
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public boolean needMaintenance() {
        return lastMaintenance >= this.getMaintenanceLimitKM();
    }

    public void performMaintenance()
            throws CannotPerformOnGroundException {
        if (onGround) {
            System.out.println("Performing maintenance");
            lastMaintenance = 0;
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public void loadModule(Equipment equipment)
            throws ModuleNotCompatibleException, NoModuleStationAvailableException {
        boolean isCompatible = false;
        if (equipments.size() >= getStationNum())
            throw new NoModuleStationAvailableException();
        else {
            for (EquipmentEnum equipmentEnum : compatibleEquipments) {
                if (equipmentEnum.getName().equals(equipment.toString()))
                    isCompatible = true;
            }

            if (isCompatible)
                equipments.add(equipment);
            else {
                throw new ModuleNotCompatibleException();
            }
        }
    }

    public void activateModule(Class<? extends Equipment> moduleClass, Coordinates targetLocation)
            throws ModuleNotFoundException, NoModuleCanPerformException {
        Optional<Equipment> equipment = equipments.stream().filter(c -> c.getClass() == moduleClass).findAny();
        if (equipment.isEmpty())
            throw new ModuleNotFoundException();
        else {
            equipment.get().activate();
            System.out.println("Successfully activate " +
                    equipment.get() + " on " +
                    targetLocation.getLongitude() + ", " +
                    targetLocation.getLatitude());
        }
    }
}