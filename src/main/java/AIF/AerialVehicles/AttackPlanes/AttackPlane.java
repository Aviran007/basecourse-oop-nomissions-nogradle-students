package AIF.AerialVehicles.AttackPlanes;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class AttackPlane extends AerialVehicle {
    public static final int MAINTENANCE_LIMIT = 10000;

    public AttackPlane(Coordinates currentLocation) {
        super(currentLocation);
    }

    public int getMaintenanceLimitKM() {
        return MAINTENANCE_LIMIT;
    }
}
