package AIF.AerialVehicles.AttackPlanes;


import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class F16 extends AttackPlane {
    private static final int STATION_NUM = 7;

    public F16(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.ATTACK, EquipmentEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.F16.getName();
    }
}
