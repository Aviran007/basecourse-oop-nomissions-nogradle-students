package AIF.AerialVehicles.AttackPlanes;


import AIF.Entities.Coordinates;
import AIF.enums.AerialVehicleEnum;
import AIF.enums.EquipmentEnum;

public class F15 extends AttackPlane {

    private static final int STATION_NUMBER = 10;

    public F15(Coordinates currentLocation) {
        super(currentLocation);
        compatibleEquipments = new EquipmentEnum[]{EquipmentEnum.ATTACK, EquipmentEnum.SENSOR};
    }

    @Override
    public int getStationNum() {
        return STATION_NUMBER;
    }

    @Override
    public String toString() {
        return AerialVehicleEnum.F15.getName();
    }
}
